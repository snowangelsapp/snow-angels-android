package com.example.aaron.snowangels;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.Random;

/**
 * Created by aaron on 2/22/2016.
 */
public class Snowflake extends ImageView {
    Handler handler;
    Context context;
    int displayWidth, displayHeight;
    RelativeLayout.LayoutParams params;
    int originalCenterX, originalCenterY, currentCenterX, currentCenterY, minX, maxX, width, height;
    int maxLeftRightWiggleRoom;
    public Snowflake(Context context)
    {
        super(context);
        this.context = context;
        handler = new Handler();
        Display display = ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        displayWidth = display.getWidth();
        displayHeight = display.getHeight();
        SetRandomSize();
        SetRandomX();
        SetParams();
    }

    public Snowflake(Context context, AttributeSet attrs)
    {
        super(context, attrs);

    }

    public Snowflake(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }
    //constants:
    // move right from original x +-2(width) to original x
    // move downwards at width/5 (=2px,4px,6px)

    private void SetRandomSize(){
        Random r = new Random();
        int max = 30;
        int min = 10;
        int rand = r.nextInt(max/10-min/10)*10 + min; // /10 and *10 for intervals of 10, may be changed to 5
        height = rand;
        width = rand;
        int x = (int) getX();
        int y = (int) getY();

        originalCenterX = x + width/2;

        //keep all snowflakes from falling off the screen
        if(originalCenterX - width*2 < 0)
            originalCenterX = width*2;
        else if(originalCenterX - width*2 > displayWidth)
            originalCenterX = displayWidth - width*2;
        originalCenterY = y + height/2;
        setLayoutParams(params);
        minX = originalCenterX - width*2;
        maxX = originalCenterX + width*2;
        maxLeftRightWiggleRoom = maxX - minX;
    }

    //Just so that all of the snowflakes dont all synchronously move left and right together
    private void SetRandomX(){
        Random r = new Random();
        int max = originalCenterX + 2*width;
        int min = originalCenterX - 2*width;
        int rand = r.nextInt(maxX-minX) + minX;
        currentCenterX = rand;
        currentCenterY = originalCenterY;
    }
    private void animateSnowflake(){
        //TODO: randomly start shifting left or right
        //TODO: create shift left Runnable
        //TODO: create shift right Runnable
        //TODO: if while shifting left- new position  == minX, start shifting Right
        //TODO: if while shifting right- new position  == maxX, start shifting Left
    }
    private int getShiftLeftRightSpeed(){
        int distanceFromcenter = Math.abs(originalCenterX - currentCenterX);
        int speed = maxLeftRightWiggleRoom/2 - distanceFromcenter;
        return speed + 1;
    }
    private void SetParams(){
        int leftMarg = currentCenterX-width/2;
        int topMarg = currentCenterY-height/2;
        int rightMarg = displayWidth - (leftMarg + width);
        int bottomMarg = displayHeight - (topMarg + height);
        params.setMargins(leftMarg,topMarg,rightMarg, bottomMarg);
        invalidate();
    }
}

