package com.example.aaron.snowangels;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class SplashScreen3 extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private static final String APP_ENGINE_API_KEY = "http://697198009954-bdebkr6hn0q75h717sqp9k88e7l9h0me.apps.googleusercontent.com/";
    private static final String MY_APP_ENGINE_API_KEY = "AIzaSyCbyy3sc6jCG4BJUTO7Mfv5N-PmJLC11yw";
    private static final String OAUTH_CLIENT_ID = "53373488917-j4nu105l09mrggn129npothlepvjhd53.apps.googleusercontent.com";
    private static final String CLIENT_SECRET = "mAH6hzrCiXqmjeHqoitpJm5v";
    private static final int RC_SIGN_IN = 101;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String REGISTER_API_URL = "http://snow-angels-server.appspot.com/register/google";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static String TAG = "LaunchActivity";
    protected String SENDER_ID = "your_sender_id";
    private GoogleCloudMessaging gcm = null;
    //
    private String regid = null;
    private String token = null;
    private Context context = null;
    Handler handler;
    ProgressDialog progressDialog;
    String email; // Received from newChooseAccountIntent(); passed to getToken()
    static final String SCOPE = "oauth2:https://www.googleapis.com/auth/userinfo.profile";
    static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    final int REQUEST_AUTHORIZATION = 42;
    GoogleApiClient googleApiClient;
    Runnable runnableGoToHomeActivity, runnablePickUser;


    private ConnectionResult connection_result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        handler = new Handler();
        //TODO: create random snowflakes
        buidNewGoogleApiClient();

        runnableGoToHomeActivity = new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen3.this, HomeActivity.class));
                SplashScreen3.this.finish();
            }
        };
        runnablePickUser = new Runnable() {
            @Override
            public void run() {
                gPlusSignIn();
            }

            ;
        };
        //if(registered (from shraredPrefs))
        //handler.postDelayed(runnableGoToHomeActivity, 3000);
        //else
        handler.postDelayed(runnablePickUser, 3000);
    }

    private void buidNewGoogleApiClient(){
        googleApiClient =  new GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API,Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
    }
 
    /*
      Customize sign-in button. The sign-in button can be displayed in
      multiple sizes and color schemes. It can also be contextually
      rendered based on the requested scopes. For example. a red button may
      be displayed when Google+ scopes are requested, but a white button
      may be displayed when only basic profile is requested. Try adding the
      Plus.SCOPE_PLUS_LOGIN scope to see the  difference.
    */



    /*
      Set on click Listeners on the sign-in sign-out and disconnect buttons
     */
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    protected void onStop() {
        super.onStop();
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    protected void onResume(){
        super.onResume();
        if (googleApiClient.isConnected()) {
            googleApiClient.connect();
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {
//        if (!result.hasResolution()) {
//            google_api_availability.getErrorDialog(this, result.getErrorCode(),request_code).show();
//            return;
//        }
//
//        if (!is_intent_inprogress) {
//
            connection_result = result;
//
//            if (is_signInBtn_clicked) {
//
//                resolveSignInError();
//            }
//        }

    }

    /*
      Will receive the activity result and check which request we are responding to
 
     */
    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent intent) {
        // Check which request we're responding to
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                Log.d("TAG", "RESULT = OK");
            }
        }
        if (!googleApiClient.isConnecting()) {
            googleApiClient.connect();
        }
    }


    @Override
    public void onConnected(Bundle arg0) {
        // Get user's information and set it into the layout
        getProfileInfo();
        // Update the UI after signin

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        googleApiClient.connect();
    }

    /*
      Sign-in into the Google + account
     */

    private void gPlusSignIn() {
        if (!googleApiClient.isConnecting()) {
            Log.d("user connected", "connected");
            resolveSignInError();

        }
    }
 
    /*
      Method to resolve any signin errors
     */

    private void resolveSignInError() {
        if (connection_result.hasResolution()) {
            try {
                //is_intent_inprogress = true;
                connection_result.startResolutionForResult(this, RC_SIGN_IN);
                Log.d("resolve error", "sign in error resolved");
            } catch (IntentSender.SendIntentException e) {
                //is_intent_inprogress = false;
                googleApiClient.connect();
            }
        }
    }
 
    /*
      Sign-out from Google+ account
     */

    private void gPlusSignOut() {
        if (googleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(googleApiClient);
            googleApiClient.disconnect();
            googleApiClient.connect();
            //changeUI(false);
        }
    }
 
    /*
     Revoking access from Google+ account
     */

    private void gPlusRevokeAccess() {
        if (googleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(googleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(googleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            Log.d("MainActivity", "User access revoked!");
                            buidNewGoogleApiClient();
                            googleApiClient.connect();
                            //changeUI(false);
                        }

                    });
        }
    }
 
    /*
     get user's information name, email, profile pic,Date of birth,tag line and about me
     */

    private void getProfileInfo() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(googleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(googleApiClient);
                setPersonalInfo(currentPerson);

            } else {
                Toast.makeText(getApplicationContext(),
                        "No Personal info mention", Toast.LENGTH_LONG).show();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
 
    /*
     set the User information into the views defined in the layout
     */

    private void setPersonalInfo(Person currentPerson){

        String personName = currentPerson.getDisplayName();
        String personPhotoUrl = currentPerson.getImage().getUrl();
        String email = Plus.AccountApi.getAccountName(googleApiClient);
//        TextView   user_name = (TextView) findViewById(R.id.userName);
//        user_name.setText("Name: "+personName);
//        TextView gemail_id = (TextView)findViewById(R.id.emailId);
//        gemail_id.setText("Email Id: " +email);
//        TextView dob = (TextView)findViewById(R.id.dob);
//        dob.setText("DOB: "+currentPerson.getBirthday());
//        TextView tag_line = (TextView)findViewById(R.id.tag_line);
//        tag_line.setText("Tag Line: " +currentPerson.getTagline());
//        TextView about_me = (TextView)findViewById(R.id.about_me);
//        about_me.setText("About Me: "+currentPerson.getAboutMe());
//        setProfilePic(personPhotoUrl);
//        progress_dialog.dismiss();
        Toast.makeText(this, "Info: \n" + personName, Toast.LENGTH_LONG).show();
    }
 
    /*
     By default the profile pic url gives 50x50 px image.
     If you need a bigger image we have to change the query parameter value from 50 to the size you want
    */
    
 
    /*
     Show and hide of the Views according to the user login status
     */

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnablePickUser);
        handler.removeCallbacks(runnableGoToHomeActivity);
    }
}
