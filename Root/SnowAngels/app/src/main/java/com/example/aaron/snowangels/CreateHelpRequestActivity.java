package com.example.aaron.snowangels;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

public class CreateHelpRequestActivity extends AppCompatActivity implements LocationListener {
    LinearLayout linearLayoutSendForHelp;
    EditText etTitle, etLiscencePlate, etMakeAndModel, etComments;
    CheckBox cbCrash, cbGas, cbStall, cbOther, cbStuck, cbPerson, cbTire;
    TextView txtLocation;
    RatingBar ratingBarUrgency;
    private String provider;
    private LocationManager locationManager;
    EditText etLocation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_help_request);
        EditText etLocation = (EditText) findViewById(R.id.etLocation);
        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the locatioin provider -> use
        // default

        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 5000, 0, this);

        provider = LocationManager.GPS_PROVIDER;
        Location lastKnownLocation = locationManager.getLastKnownLocation(provider);
        Location location = locationManager.getLastKnownLocation(provider);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        } else {
            if (location != null) {
                long latitude = (long) (location.getLatitude());
                long longitude = (long) (location.getLongitude());
                etLocation.setText(String.valueOf(latitude) + ", " + String.valueOf(longitude));
            }
            if (lastKnownLocation != null) {
                long latitude = (long) (location.getLatitude());
                long longitude = (long) (location.getLongitude());
                etLocation.setText(String.valueOf(latitude) + ", " + String.valueOf(longitude));
            }
        }
    }
    @Override
    public void onLocationChanged(Location loc) {
        double longitude = loc.getLongitude();
        double latitude = loc.getLatitude();
        etLocation.setText(String.valueOf(latitude) + ", " + String.valueOf(longitude));
    }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
}

