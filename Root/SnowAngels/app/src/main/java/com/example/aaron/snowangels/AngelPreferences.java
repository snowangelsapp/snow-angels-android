package com.example.aaron.snowangels;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class AngelPreferences extends AppCompatActivity {
    LinearLayout linearLayoutNotificationAnyTime, linearLayoutDoNotNotifyBetween, linearLayoutEnableFacebookNotifciations,
        linearLayoutEnableTwitterNotifications, linearLayoutEnableEmailNotifications, linearLayoutSavePreferencesButton;
    Switch switchNotificationsAnyTime, switchEnableFacebookNotifications, switchEnableTwitterNotifications, switchEnableEmailNotifications;
    TextView txtStartTime, txtEndTime;
    CheckBox cbCrash, cbGas, cbStall, cbOther, cbStuck, cbPerson, cbTire;

    GregorianCalendar startCal, endCal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_angel_preferences);
        linearLayoutNotificationAnyTime = (LinearLayout) findViewById(R.id.linearLayoutNotifciationsAnyTime);
        linearLayoutDoNotNotifyBetween = (LinearLayout) findViewById(R.id.linearLayoutDoNotNotifyBetween);
        linearLayoutEnableFacebookNotifciations = (LinearLayout) findViewById(R.id.linearLayoutEnableFacebookNotifications);
        linearLayoutEnableTwitterNotifications = (LinearLayout) findViewById(R.id.linearLayoutEnableTwitterNotifications);
        linearLayoutEnableEmailNotifications = (LinearLayout) findViewById(R.id.linearLayoutEnableEmailNotifications);
        linearLayoutSavePreferencesButton = (LinearLayout) findViewById(R.id.linearLayoutSavePreferencesButton);

        switchNotificationsAnyTime = (Switch) findViewById(R.id.switchNotificationsAnyTime);
        switchEnableFacebookNotifications = (Switch) findViewById(R.id.switchEnableFacebookNotifications);
        switchEnableTwitterNotifications = (Switch) findViewById(R.id.switchEnableTwitterNotifications);
        switchEnableEmailNotifications = (Switch) findViewById(R.id.switchEnableEmailNotifications);

        switchNotificationsAnyTime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    linearLayoutDoNotNotifyBetween.setVisibility(View.GONE);
                else
                    linearLayoutDoNotNotifyBetween.setVisibility(View.VISIBLE);
            }
        });
        linearLayoutNotificationAnyTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchNotificationsAnyTime.toggle();
            }
        });
        linearLayoutEnableFacebookNotifciations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchEnableFacebookNotifications.toggle();
            }
        });
        linearLayoutEnableTwitterNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchEnableTwitterNotifications.toggle();
            }
        });
        linearLayoutEnableEmailNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchEnableEmailNotifications.toggle();
            }
        });

        txtStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int hour, minute;
                hour = startCal.get(Calendar.HOUR_OF_DAY);
                minute = startCal.get(Calendar.MINUTE);
                TimePickerDialog tpd = new TimePickerDialog(AngelPreferences.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                startCal.set(Calendar.MINUTE, minute);
                                startCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                if (startCal.getTimeInMillis() < new GregorianCalendar().getTimeInMillis())
                                    startCal.add(Calendar.DAY_OF_MONTH, 1);
                                txtStartTime.setText(GetTimeString(startCal));
                            }
                        }, hour, minute, false);
                tpd.updateTime(hour, minute);
                tpd.show();
            }
        });
        txtEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int hour, minute;
                hour = endCal.get(Calendar.HOUR_OF_DAY);
                minute = endCal.get(Calendar.MINUTE);
                TimePickerDialog tpd = new TimePickerDialog(AngelPreferences.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                endCal.set(Calendar.MINUTE, minute);
                                endCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                if (endCal.getTimeInMillis() < new GregorianCalendar().getTimeInMillis())
                                    endCal.add(Calendar.DAY_OF_MONTH, 1);
                                txtEndTime.setText(GetTimeString(endCal));
                            }
                        }, hour, minute, false);
                tpd.updateTime(hour, minute);
                tpd.show();
            }
        });
        linearLayoutSavePreferencesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(AngelPreferences.this, "Thank you for becoming an Angel!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(AngelPreferences.this, HomeActivity.class));
            }
        });
    }

    public String GetTimeString(GregorianCalendar c) {
        String ampm;
        int hours, minutes = (c.get(Calendar.MINUTE));
        if (c.get(Calendar.HOUR) == 0)
            hours = 12;
        else
            hours = c.get(Calendar.HOUR);
        if (c.get(Calendar.AM_PM) == 1)
            ampm = "PM";
        else
            ampm = "AM";
        return String.format("%02d:%02d %s", hours, minutes, ampm);
    }
}
