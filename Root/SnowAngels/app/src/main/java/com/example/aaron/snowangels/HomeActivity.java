package com.example.aaron.snowangels;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Button btnINeedHelp = (Button)findViewById(R.id.btnINeedHelp);
        Button btnBecomeAnAngel = (Button)findViewById(R.id.btnBecomeAnAngel);
        Button btnOurCommunity = (Button)findViewById(R.id.btnOurCommunity);
        btnINeedHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this)
                        .setTitle("Warning!")
                        .setMessage("If you are in an emergency or are in any danger, please call the authorities")
                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(HomeActivity.this, CreateHelpRequestActivity.class));
                            }
                        }).
                        setNeutralButton("Open Phone", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Uri uri = Uri.parse("tel:911");
                                Intent intent = new Intent (Intent.ACTION_DIAL, uri);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog.dismiss();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }
}
