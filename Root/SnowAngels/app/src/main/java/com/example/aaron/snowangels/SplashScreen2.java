package com.example.aaron.snowangels;

import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class SplashScreen2 extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{
    private static final String APP_ENGINE_API_KEY = "http://697198009954-bdebkr6hn0q75h717sqp9k88e7l9h0me.apps.googleusercontent.com/";
    private static final String MY_APP_ENGINE_API_KEY = "AIzaSyCbyy3sc6jCG4BJUTO7Mfv5N-PmJLC11yw";
    private static final String OAUTH_CLIENT_ID = "53373488917-j4nu105l09mrggn129npothlepvjhd53.apps.googleusercontent.com";
    private static final String CLIENT_SECRET = "mAH6hzrCiXqmjeHqoitpJm5v";
    private static final int RC_SIGN_IN = 101;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String REGISTER_API_URL = "http://snow-angels-server.appspot.com/register/google";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static String TAG = "LaunchActivity";
    protected String SENDER_ID = "your_sender_id";
    private GoogleCloudMessaging gcm = null;
    //
    private String regid = null;
    private String token = null;
    private Context context = null;
    Handler handler;
    ProgressDialog progressDialog;
    String email; // Received from newChooseAccountIntent(); passed to getToken()
    static final String SCOPE = "oauth2:https://www.googleapis.com/auth/userinfo.profile";
    static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    final int REQUEST_AUTHORIZATION = 42;
    GoogleApiClient googleApiClient;
    Runnable runnableGoToHomeActivity, runnablePickUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        handler = new Handler();
        //TODO: create random snowflakes

        runnableGoToHomeActivity = new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen2.this, HomeActivity.class));
                SplashScreen2.this.finish();
            }
        };
        runnablePickUser = new Runnable() {
            @Override
            public void run() {
                pickUserAccount();
            }
        };
        //if(registered (from shraredPrefs))
        //handler.postDelayed(runnableGoToHomeActivity, 3000);
        //else
        handler.postDelayed(runnablePickUser, 3000);
    }

    private void pickUserAccount() {
        Log.d(TAG, "Starting pickUserAccount");
        String serverClientId = OAUTH_CLIENT_ID;
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(serverClientId)
                .requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        googleApiClient.connect();
        Log.d(TAG, "Finishing pickUserAccount");
        signIn();
    }
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        else if (resultCode == RESULT_CANCELED) {
            // The account picker dialog closed without selecting an account.
            // Notify users that they must pick an account to proceed.
            Toast.makeText(this, "You need to register an account to use this app", Toast.LENGTH_SHORT).show();
        }
    }
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        Log.d(TAG, "handleSignInResult status:" + result.getStatus());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String authCode = acct.getServerAuthCode();

            onEmailReceived();

            new RegisterWithServer().execute();
        } else {
            // Signed out, show unauthenticated UI.
        }
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {

            }
        });
    }


    /**
     * Gets an authentication token from Google and handles any
     * GoogleAuthException that may occur.
     */
    private void onEmailReceived() {
        context = getApplicationContext();
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);

            if (regid.isEmpty()) {
                registerInBackground();
            } else {
                Log.d("TAG", "No valid Google Play Services APK found.");
            }
        }
        //TODO: check Database to see if the user is already registered
        //TODO: if the user is already registered, suggest to log into it.
        //TODO: else, prompt the user to register

    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.d(TAG, "Registration ID not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.d(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(SplashScreen2.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.d(TAG, "This device is not supported - Google Play Services.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void registerInBackground() {
        new registerWithGCM().execute(null, null, null);
    }

    private class registerWithGCM extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            String msg = "";
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(context);
                }
                regid = gcm.register(SENDER_ID);
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            //to do here };
        }
    }

    private class RegisterWithServer extends AsyncTask<Void, Void, String> {
        /**
         * Executes the asynchronous job. This runs when you call execute()
         * on the AsyncTask instance.
         */
        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL(REGISTER_API_URL);

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("auth", token);
                jsonObject.put("email", email);
                jsonObject.put("registration_id", regid);


                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(5000);
                conn.setConnectTimeout(5000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                String data = jsonObject.toString();
                OutputStreamWriter out = new   OutputStreamWriter(conn.getOutputStream());
                out.write(data);
                System.out.print("\n");
                System.out.print(data);
                System.out.print("\n");
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(data);
                writer.flush();
                writer.close();
                int response = conn.getResponseCode();
                String errorMessage = conn.getResponseMessage();
                System.out.println("Response: " + response);
                System.out.println("Response Error: " + errorMessage);
                os.close();

                conn.connect();
            }catch (Exception e){
                e.printStackTrace();
                return e.toString();
            }
            return "Error-less on the app side";
        }

        @Override
        protected void onPostExecute(String string) {
            System.out.print("\n");
            System.out.print(string);
            System.out.print("\n");
            super.onPostExecute(string);
            Toast.makeText(SplashScreen2.this, string, Toast.LENGTH_SHORT).show();
            startActivity(new Intent(SplashScreen2.this, HomeActivity.class));
            SplashScreen2.this.finish();
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // TODO Auto-generated method stub
    }
    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnablePickUser);
        handler.removeCallbacks(runnableGoToHomeActivity);
    }
}
