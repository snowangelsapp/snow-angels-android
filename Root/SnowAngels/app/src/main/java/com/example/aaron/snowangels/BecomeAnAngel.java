package com.example.aaron.snowangels;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

public class BecomeAnAngel extends AppCompatActivity {

    LinearLayout linearLayoutSavePreferencesButton, linearLayoutAddPhotoButton;
    EditText etName, etAddress1, etAddress2, etCity, etZipCode, etPhoneNumber, etMakeAndModel;
    Spinner spinnerState;
    ImageView imgProfilePicture;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_an_angel);
        linearLayoutSavePreferencesButton = (LinearLayout) findViewById(R.id.linearLayoutSavePreferencesButton);
        linearLayoutAddPhotoButton = (LinearLayout) findViewById(R.id.linearLayoutAddPhotoButton);
        etName = (EditText) findViewById(R.id.etName);
        etAddress1 = (EditText) findViewById(R.id.etAddress1);
        etAddress2 = (EditText) findViewById(R.id.etAddress2);
        etCity = (EditText) findViewById(R.id.etCity);
        etZipCode = (EditText) findViewById(R.id.etZipCode);
        etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        etMakeAndModel = (EditText) findViewById(R.id.etMakeAndModel);
        spinnerState = (Spinner) findViewById(R.id.spinnerState);
        imgProfilePicture = (ImageView) findViewById(R.id.imgProfilePicture);
        
        linearLayoutSavePreferencesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BecomeAnAngel.this, AngelPreferences.class));
            }
        });
    }
}
