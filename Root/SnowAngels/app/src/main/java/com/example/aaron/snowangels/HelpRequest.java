package com.example.aaron.snowangels;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by aaron on 2/24/2016.
 */
public class HelpRequest {
    public static final int REQUEST_TYPE_CRASH = 0;
    public static final int REQUEST_TYPE_STALL = 1;
    public static final int REQUEST_TYPE_GAS = 2;
    public static final int REQUEST_TYPE_STUCK = 3;
    public static final int REQUEST_TYPE_TIRE = 4;
    public static final int REQUEST_TYPE_PERSON = 5;
    public static final int REQUEST_TYPE_OTHER = 6;
    int type;
    LatLng location;
    String title;
    String liscencePlate;
    String Make;
    String Model;
    String Comment;
    int urgency;



}
